import 'package:doc_widget/doc_widget.dart';
import 'package:flutter/material.dart';

@docWidget
class RTButton extends StatelessWidget {
  const RTButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(onPressed: () {}, child: const Text('tombol'));
  }
}
